/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to check app type
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
  if (sails.config.appList.indexOf(req.headers["x-app"]) === -1 &&
    sails.config.hosts.indexOf(req.host) === -1) {
      sails.log.error("App:", req.headers["x-app"], "not allowed.");
      return res.serverError("Invalid app");
  }

  req.appType = req.headers["x-app"];
  next();
};
