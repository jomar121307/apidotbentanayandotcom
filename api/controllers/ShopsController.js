/**
 * ShopsController
 *
 * @description :: Server-side logic for managing Shops
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var ObjectID = require("objectid");
 var requestHttp = require("request");

module.exports = {
	create : function (req, res) {
    return res.notFound();
  },

  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    if (req.query.random === "true") {
      return Shops.findRandom({
        userId : req.userId,
        appType : req.appType,
        where : req.query.where,
        skip : req.query.skip,
        limit : req.query.limit,
        sort : req.query.sort
      }, function (err, payload) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        payload.shops = _.map(payload.shops, function (shop) {
          shop.userId = shop.userId.sanitize();
          return shop.sanitize(req.appType);
        });

        res.ok(payload);
      });
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Maformed query.");
    }

    _.extend(req.query.where, {
      isDeleted : false
    });

    async.waterfall([
      function (waterfallCb) {
        Shops.count()
          .where(req.query.where)
          .exec(function (err, count) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, {
              count : count,
              shops : []
            });
          });
      },
      function (payload, waterfallCb) {
        if (!payload.count) {
          return process.nextTick(function () {
            waterfallCb(null, payload);
          });
        }

        Shops.find(req.query)
          .populate("userId")
          .exec(function (err, foundShops) {
            if (err) {
              return waterfallCb(err);
            }

            async.map(foundShops, function (shop, mapCb) {
              shop.userId = shop.userId.sanitize();
              shop.populateCurrency({
                appType : req.appType
              }, function (err) {
                if (err) {
                  return mapCb(err);
                }

                mapCb(null, shop.sanitize(req.appType));
              });
            }, function (err, mapResult) {
              if (err) {
                return waterfallCb(err);
              }

              payload.shops = mapResult;
              waterfallCb(null, payload);
            });
          });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  findOne : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    async.waterfall([
      function (waterfallCb) {
        Shops.native(function (err, collection) {
          if (err) {
            return waterfallCb(err);
          }
          var query = {};

          if (ObjectID.isValid(req.params.id)) {
            query._id = new ObjectID(req.params.id);
          } else {
            query["metadata." + req.appType + ".shopName"] = req.params.id;
          }

          collection.findOne(query, function (err, foundShop) {
            if (err) {
              return waterfallCb(err);
            } else if (!foundShop) {
              return waterfallCb({
                type : "notFound",
                msg : "Shop not found."
              })
            }

            waterfallCb(null, foundShop);
          });
        });
      },
      function (foundShop, waterfallCb) {
        Shops.findOneById(foundShop._id)
          .populate("userId")
          .exec(function (err, foundShop) {
            if (err) {
              return waterfallCb(err);
            } else if (!foundShop) {
              return waterfallCb({
                type : "notFound",
                msg : "Shop not found."
              });
            }

            waterfallCb(null, foundShop);
          });
      },
      function (foundShop, waterfallCb) {
        if (req.query.products !== "true") {
          return process.nextTick(function () {
            waterfallCb(null, foundShop);
          });
        }

        foundShop.fetchProducts({
          appType : req.appType,
          limit : req.query.limit,
          skip : req.query.skip,
          sort : req.query.sort,
        }, function (err) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, foundShop);
        });
      },
      function (foundShop, waterfallCb) {
        foundShop.populateCurrency({
          appType : req.appType
        }, function (err) {
          if (err) {
            return waterfallCb(err);
          }

          foundShop.userId = foundShop.userId.sanitize();
          waterfallCb(null, foundShop.sanitize(req.appType));
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  shopSettings : function (req, res) {
    Shops.findOne({
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, shop) {
      if (err) {
        return res.serverError(err);
      } else if (!shop) {
        sails.log.error("ShopsController :: shopSettings :: Shop not found.");
        return res.serverError("Error");
      }

      res.ok({
        paypalInfo : shop.metadata[req.appType].paypalInfo,
        eCashInfo : shop.metadata[req.appType].eCashInfo,
        shopName : shop.metadata[req.appType].shopName
      });
    });
  },

  saveShopSettings : function (req, res) {
    var shopName = req.body.shopName;
    var eCashInfo = req.body.eCashInfo;
    var paypalInfo = req.body.paypalInfo;

    async.series([
      function (seriesCb) {
        if (_.isEmpty(eCashInfo)) {
          return process.nextTick(seriesCb);
        }

        requestHttp.post({
          url : sails.config.ecash.loginUrl,
          formData : {
            api_token : sails.config.ecash.apiToken,
            api_key : sails.config.ecash.apiKey,
            username : eCashInfo.username,
            password : eCashInfo.password
          }
        }, function (err, result, body) {
          if (err) {
            return seriesCb(err);
          }

          sails.log.debug(body);

          try {
            body = JSON.parse(body);
          } catch (e) {
            sails.log.error("ShopsController :: saveShopSettings ::", e.message);
            return seriesCb("Error.");
          }

          if (!body.S) {
            return seriesCb(body.M);
          }

          delete eCashInfo.password;
          eCashInfo.merchantId = body.merchant_id;
          seriesCb();
        });
      },
      function (seriesCb) {
        // Disable all accounts to set paypal info, thus preventing paypal payments
        // if (_.isEmpty(paypalInfo)) {
          return process.nextTick(seriesCb);
        // }

        PaypalService.connect({
          firstName : paypalInfo.name.firstName,
          lastName : paypalInfo.name.lastName,
          emailAddress : paypalInfo.emailAddress
        }, function (err, result) {
          if (err) {
            return seriesCb(err);
          } else if (result.code) {
            accountObj = result;
            return seriesCb("accountError")
          }

          sails.log.verbose("Paypal account info.", result);

          if (result.userInfo.emailAddress !== paypalInfo.emailAddress) {
            result.userInfo.emailAddress = paypalInfo.emailAddress;
          }

          paypalInfo = result.userInfo;
          seriesCb();
        });
      },
      function (seriesCb) {
        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
          if (err) {
            return waterfallCb(err);
          } else if (!shop) {
            sails.log.error("ShopsController :: saveShopSettings :: Shop not found.");
            return waterfallCb("Error");
          }

          if (paypalInfo) {
            shop.metadata[req.appType].paypalInfo = paypalInfo;
            sails.log.debug(shop.metadata[req.appType].paypalInfo, paypalInfo);
          }

          if (eCashInfo) {
            shop.metadata[req.appType].eCashInfo = eCashInfo;
          }

          if (shopName) {
            shop.metadata[req.appType].shopName = shopName;
          }

          shop.save(function (err) {
            if (err) {
              return seriesCb(err);
            }

            seriesCb();
          });
        });
      }
    ], function (err) {
      if (err && res.hasOwnProperty(err.type)) {
        return res[err.type](err.msg);
      } else if (err) {
        return res.serverError(err);
      }

      res.ok({
        status : "success"
      });
    });
  },

  setCheckoutInfo : function (req, res) {
    Shops.findOne({
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, shop) {
      if (err) {
        return res.serverError(err);
      } else if (!shop) {
        sails.log.error("ShopsController :: setCheckoutInfo :: Shop not found.");
        return res.serverError("Error.");
      }

      _.ensure(shop, "metadata." + req.appType + ".checkoutInfo", {});
      _.merge(shop.metadata[req.appType].checkoutInfo, req.body);

      shop.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        res.ok({
          status : "success"
        });
      });
    });
  },
};

