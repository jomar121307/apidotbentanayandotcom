/**
 * ReviewsController
 *
 * @description :: Server-side logic for managing Reviews
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function (req, res) {
    delete req.body.isDeleted;

    req.body.userId = req.userId;
    req.body.appType = req.appType;

    Reviews.create(req.body)
      .exec(function (err, createdReview) {
        if (err) {
          return res.serverError(err);
        }

        res.ok(createdReview.sanitize());
      });
  },

  update : function (req, res) {
    Reviews.findOne({
      id : req.params.id,
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, foundReview) {
      if (err) {
        return res.serverError(err);
      } else if (!foundReview) {
        return res.notFound("Review not found.");
      }

      foundReview.content = req.body.content;
      foundReview.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        res.ok(foundReview.sanitize());
      });
    });
  },

  destroy : function (req, res) {
    Reviews.findOne({
      id : req.params.id,
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, foundReview) {
      if (err) {
        return res.serverError(err);
      } else if (!foundReview) {
        return res.notFound("Review not found.");
      }

      foundReview.isDeleted = true;
      foundReview.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        res.ok(foundReview.sanitize());
      });
    });
  }
};

