/**
 * CartController
 *
 * @description :: Server-side logic for managing Carts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create : function (req, res) {
    try {
      req.body = updateVerifier(Cart, req.body);
    } catch (e) {
      return res.badRequest(e.message);
    }

    _.extend(req.body, {
      userId : req.userId,
      appType : req.appType
    });

    async.waterfall([
      function (waterfallCb) {
        Products.findOne({
          id : req.body.productId,
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, foundProduct) {
          if (err) {
            return waterfallCb(err);
          } else if (!foundProduct) {
            return waterfallCb({
              type : "notFound",
              msg : "Product not found."
            });
          } else if (foundProduct.quantity < req.body.quantity) {
            return waterfallCb({
              type : "badRequest",
              msg : "Insufficient product quantity."
            });
          }

          req.body.shopId = foundProduct.shopId;
          waterfallCb(null, foundProduct);
        });
      },
      function (foundProduct, waterfallCb) {
        try {
          req.body.quantity = parseInt(req.body.quantity);
        } catch (e) {
          return res.badRequest(e.message);
        }

        Cart.findOne({
          productId : req.body.productId,
          userId : req.userId,
          status : "added",
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, foundItem) {
          if (err) {
            return waterfallCb(err);
          } else if (foundItem &&
            foundProduct.quantity < foundItem.quantity + req.body.quantity) {
            return waterfallCb({
              type : "badRequest",
              msg : "Insufficient product quantity."
            });
          } else if (foundItem) {
            foundItem.quantity += req.body.quantity;
            return foundItem.save(function (err) {
              if (err) {
                return waterfallCb(err);
              }

              waterfallCb(null, foundItem.sanitize());
            });
          }

          Cart.create(req.body)
            .exec(function (err, createdItem) {
              if (err) {
                return waterfallCb(err);
              }

              waterfallCb(null, createdItem.sanitize());
            });
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Malformed query.");
    }

    _.extend(req.query.where, {
      userId : req.userId,
      status : "added",
      isDeleted : false,
      appType : req.appType
    });

    async.waterfall([
      function (waterfallCb) {
        if (!req.query.payment) {
          return process.nextTick(function () {
            waterfallCb(null, false);
          });
        }

        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
            if (err) {
              return waterfallCb({
                type : "serverError",
                msg : err
              });
            } else if (!shop) {
              sails.log.error("CartController :: find :: Shop not found for user:", req.userId);
              return waterfallCb("Error");
            } else if (!_.safe(shop,
              ["metadata.", req.appType, ".checkoutInfo", ".checkoutShopId"].join(""))) {
              sails.log.error("CartController :: find :: Proceeded to payment route without shopId.");
              return waterfallCb("Error");
            }

            req.query.where.shopId = shop.metadata[req.appType].checkoutInfo.checkoutShopId;
            waterfallCb(null, shop);
          });
      },
      function (shop, waterfallCb) {
        if (!req.query.payment) {
          return process.nextTick(function () {
            waterfallCb(null, false, false);
          });
        } else if (!_.safe(shop, ["metadata.", req.appType, ".checkoutInfo", ".shippingId"].join(""))) {
          return process.nextTick(function () {
            sails.log.error("CartController :: find :: Proceeded to payment route without shippingId.");
            waterfallCb("Error");
          });
        }

        Shipping.findOne({
          id : shop.metadata[req.appType].checkoutInfo.shippingId,
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, foundAddress) {
          if (err) {
            return waterfallCb(err);
          } else if (!foundAddress) {
            return waterfallCb("Shipping address not found.");
          }

          waterfallCb(null, foundAddress, shop.metadata[req.appType].checkoutInfo.paymentPlatform);
        });
      },
      function (foundAddress, paymentPlatform, waterfallCb) {
        Cart.count()
          .where(req.query.where)
          .exec(function (err, count) {
            if (err) {
              return waterfallCb(err);
            } else if (!count) {
              return waterfallCb(null, {
                count : 0,
                items : []
              });
            }

            waterfallCb(null, {
              paymentPlatform : paymentPlatform,
              address : foundAddress,
              count : count,
              items : []
            })
          });
      },
      function (payload, waterfallCb) {
        if (!payload.address) delete payload.address;
        if (!payload.paymentPlatform) delete payload.paymentPlatform;

        Cart.find(req.query)
          .populateAll()
          .exec(function (err, foundItems) {
            if (err) {
              return waterfallCb(err);
            }

            payload.items = foundItems;
            waterfallCb(null, payload);
          });
      },
      function (payload, waterfallCb) {
        async.each(payload.items, function (item, eachCb) {
          item.fetchSeller({}, eachCb);
        }, function (err) {
          if (err) {
            return waterfallCb(err);
          }

          payload.items = _.map(payload.items, function (item) {
            const paymentPlatformsAvail = [];

            _.each(item.shopId.metadata[req.appType], function (v, k) {
              switch (k) {
                case "paypalInfo":
                  paymentPlatformsAvail.push("paypal");
                  break;
                case "eCashInfo":
                  paymentPlatformsAvail.push("ecash");
                  break;
              }
            });

            item.userId = item.userId.profileSanitize();
            item.productId = item.productId.sanitize();
            item.shopId = item.shopId.sanitize(req.appType);
            item.productId.userId.paymentPlatformsAvailable = paymentPlatformsAvail;
            return item.sanitize();
          });

          waterfallCb(null, payload);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  findOne : function (req, res) {
    res.notFound();
  },

  update : function (req, res) {
    req.body.quantity = parseInt(req.body.quantity);
    if (!(req.body.quantity && _.isNumber(req.body.quantity))) {
      return res.badRequest("Quantity is not a number.");
    }

    Cart.findOne({
      id : req.params.id,
      userId : req.userId,
      appType : req.appType,
      isDeleted : false
    }).populate("productId")
      .exec(function (err, foundItem) {
      if (err) {
        return res.serverError(err);
      } else if (!foundItem) {
        return res.notFound("Cart item not found.");
      } else if (_.safe(foundItem, "productId.quantity") < req.body.quantity) {
        return res.badRequest("Insufficient product quantity.");
      }

      foundItem.quantity = req.body.quantity;
      foundItem.save(function (err) {
        if (err) {
          return  res.serverError(err);
        }

        foundItem.productId = foundItem.productId.sanitize();
        res.ok(foundItem.sanitize());
      });
    });
  },

  destroy : function (req, res) {
    var query = {
      id : req.params.id,
      userId : req.userId,
      appType : req.appType,
      isDeleted : false
    };

    if (req.query.shop === "true") {
      delete query.id;
      query.shopId = req.params.id;
    }

    Cart.findOne(query)
      .exec(function (err, foundItem) {
        if (err) {
          return res.serverError(err);
        } else if (!foundItem) {
          return res.notFound("Cart item not found.");
        }

        foundItem.isDeleted = true;
        foundItem.save(function (err) {
          if (err) {
            return res.serverError(err);
          }

          res.ok(foundItem.sanitize());
        });
    });
  },

  empty : function (req, res) {
    var query = {
      userId : req.userId,
      appType : req.appType,
      status : "added",
      isDeleted : false
    };

    Cart.count(query)
      .exec(function (err, count) {
        if (err) {
          return res.serverError(err);
        } else if (!count) {
          return res.ok({
            count : 0,
            items : []
          });
        }

        Cart.find(query)
          .exec(function (err, items) {
            if (err) {
              return res.serverError(err);
            }

            async.map(items, function (item, cb) {
              item.isDeleted = true;
              item.save(function (err) {
                if (err) {
                  return cb(err);
                }

                cb(null, item.sanitize());
              });
            }, function (err, result) {
              if (err) {
                if (res.hasOwnProperty(err.type)) {
                  return res[err.type](err.msg);
                }

                return res.serverError(err);
              }

              res.ok({
                count : count,
                items : result
              });
            });
        });
    });
  },

  confirmPayments : function (req, res) {

  },

  gonnaPayNow : function (req, res) {
    PlatformService.charge(req, res);
  }
};
