/**
 * OrdersController
 *
 * @description :: Server-side logic for managing Orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Malformed query.");
    }

    delete req.query.transactionId;
    delete req.query.payKey;

    _.extend(req.query.where, {
      userId : req.userId,
      appType : req.appType,
      isCompleted : true,
      isDeleted : false
    });

    async.waterfall([
      function (waterfallCb) {
        if (!req.query.dogmode) {
          return process.nextTick(waterfallCb);
        }

        Users.findOne({
          id : req.userId,
          isAdmin : true
        }).exec(function (err, admin) {
          if (err) {
            return waterfallCb(err);
          } else if (!admin) {
            sails.log.error("OrdersController :: find :: Non-admin attemped to query all orders.");
            return waterfallCb("Error");
          }

          waterfallCb();
        });
      },
      function (waterfallCb) {
        if (req.query.purchases || req.query.dogmode) {
          return process.nextTick(waterfallCb);
        }

        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
          if (err) {
            return waterfallCb(err);
          } else if (!shop) {
            sails.log.error("OrdersController :: find :: Shop not found.");
            return waterfallCb("error");
          }

          delete req.query.where.userId;
          req.query.where.shopId = shop.id;

          waterfallCb();
        });
      },
      function (waterfallCb) {
        if (req.query.dogmode) {
          delete req.query.where.userId;
        }

        Orders.count()
          .where(req.query.where)
          .exec(function (err, count) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, {
              count : count,
              orders : []
            });
          });
      },
      function (payload, waterfallCb) {
        if (!payload.count) {
          return process.nextTick(function () {
            waterfallCb(null, payload);
          });
        }

        Orders.find(req.query)
          .populateAll()
          .exec(function (err, orders) {
            if (err) {
              return waterfallCb(err);
            }

            payload.orders = _.map(orders, function (o) {
              return o.sanitize();
            });

            waterfallCb(null, payload);
          });
      },
      function (payload, waterfallCb) {
        async.map(payload.orders, function (order, mapCb) {
          Shops.findOneById(order.shopId.id)
            .populateAll()
            .exec(function (err, shop) {
              if (err) {
                return mapCb(err);
              }

              order.shopId = shop.sanitize(req.appType);
              mapCb(null, order);
            });
        }, function (err, result) {
          if (err) {
            return waterfallCb(err);
          }

          payload.orders = result;
          waterfallCb(null, payload);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  findOne : function (req, res) {

  },

  updateStatus : function (req, res) {
    async.waterfall([
      function (waterfallCb) {
        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, shop.id);
        });
      },
      function (shopId, waterfallCb) {
        Orders.findOne({
          id : req.params.id,
          // status : {
          //   not : "refunded"
          // },
          shopId : shopId,
          appType : req.appType,
          isCompleted : true,
          isDeleted : false
        }).populateAll()
          .exec(function (err, order) {
          if (err) {
            return waterfallCb(err);
          } else if (!order) {
            return waterfallCb({
              type : "notFound",
              msg : "Order not found."
            });
          } else if (order.status === req.body.status) {
            return waterfallCb({
              type : "badRequest",
              msg : "Same status"
            });
          } else if (req.body.status === "refunded") {
            return waterfallCb(null, order);
          }

          order.status = req.body.status;
          order.save(function (err) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, order);
          });
        });
      },
      function (order, waterfallCb) {
        if (req.body.status !== "refunded") {
          return process.nextTick(function () {
            waterfallCb(null, order);
          });
        }

        PaypalService.refund({
          payKey : order.payKey
        }, function (err, data) {
          if (err) {
            return waterfallCb(err);
          }

          var msg = _.safe(data, "refundInfoList.refundInfo.0.refundStatus");
          switch(msg) {
            case "REFUNDED":
              order.status = "refunded";
              return waterfallCb(null, order);
            case "REFUNDED_PENDING":
              return waterfallCb("Refund awaiting transfer of funds");
            case "NO_API_ACCESS_TO_RECEIVER":
              return waterfallCb(null, {
                message : "No api access."
              });
            case "REFUND_NOT_ALLOWED":
              return waterfallCb("Refund not allowed.");
            case "INSUFFICIENT_BALANCE":
              return waterfallCb("Insufficient balance.");
            case "AMOUNT_EXCEEDS_REFUNDABLE":
              return waterfallCb("Amount exceeds refundable.");
            case "PREVIOUS_REFUND_PENDING":
              return waterfallCb("Refund is pending.");
            case "ALREADY_REVERSED_OR_REFUNDED":
            case "NOT_PAID":
            case "NOT_PROCESSED":
            case "REFUND_ERROR":
            case "PREVIOUS_REFUND_ERROR":
              sails.log.info(require("util").inspect(data, {depth : null}));
              sails.log.error("OrdersController :: changeStatus :: PaypalRefund ::", msg);
              return waterfallCb("Error");
            default:
              sails.log.info(require("util").inspect(data, {depth : null}));
              sails.log.error("OrdersController :: changeStatus :: PaypalRefund :: Message not handled :: ", msg)
              return waterfallCb("Error");
          }
        });
      },
      function (order, waterfallCb) {
        if (order.message === "No api access.") {
          return process.nextTick(function () {
            waterfallCb(null, order);
          });
        } else if (order.status !== "refunded") {
          return process.nextTick(function () {
            waterfallCb(null, order.sanitize());
          });
        }

        order.save(function (err) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, order.sanitize());
        });
      }
    ], function (err, result) {
      if (err && res.hasOwnProperty(err.type)) {
        return res[err.type](err.msg);
      } else if (err) {
        return res.serverError(err);
      }

      res.ok(result);
    });
  }
};

