var sendgrid  = require('sendgrid')(sails.config.email.sendgrid.apiKey);

module.exports = {
  sendMail : function (email, cb) {
    sendgrid.send(email, cb);
  }
}
