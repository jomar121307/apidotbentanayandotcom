/**
 * The hook assumes the values parameter has been validated.
 *
 * @param {object} values
 * @param {function} nodejs callback
 *
 */

var ObjectID = require("objectid");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Hook :: Global :: Messages");
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Hooks :: Global :: Messages :: App type is undefined");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        process.nextTick(seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }
    });
  }

  async.series([
    function (seriesCb) {
      incrementMessageCount(values, seriesCb);
    },
    function (seriesCb) {
      createNotification(values, seriesCb);
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Increment message counter in room
 *
 * @method     incrementMessageCount
 * @param      {<type>}    values    { description }
 * @param      {Function}  callback  { description }
 */
function incrementMessageCount (values, callback) {
  Rooms.native(function (err, collection) {
    if (err) {
      return callback(err);
    }

    collection.findOneAndUpdate({
      _id : ObjectID(values.roomId),
      isDeleted : false
    }, {
      $inc : {
        messageCount : 1,
        unreadCount : 1
      },
      $set : {
        updatedAt : new Date()
      }
    }, function (err, result) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  });
}

/**
 * Create notifications to receiver for this message
 *
 * @method     createNotification
 * @param      {<type>}    values    { description }
 * @param      {Function}  callback  { description }
 */
function createNotification (values, callback) {
  Notifications.create({
    entity : values.roomId,
    entityType : "message",
    notifiedId : values.to,
    notifierId : values.from,
    action : "message",
    appType : values.appType
  }).exec(function (err, notif) {
    if (err) {
      return callback(err);
    }

    callback();
  });
}
