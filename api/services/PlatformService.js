var base36 = require("monotonic-timestamp-base36");
var underscore = require("underscore");
var requestHttp = require("request");

module.exports = {
  getFee : function (amount) {
    var fee = 0;

    if (amount < 0) {
      throw new Error("Amount is less than zero.");
    }

    fee = Math.ceil(amount * sails.config.zcommerce.platformFee.percentage) / 100;
    fee += sails.config.zcommerce.platformFee.flat;

    return fee;
  },

  charge : function (req, res) {
    var self = this;
    var userId = req.userId;

    var shippingId = null;
    var selectedShop = null;
    var card = null;
    var cartItems = null;
    var costsByShop = null;
    var receivers = null;
    var paymentData = null;
    var shopCurrency = null;
    var paymentPlatform = "";
    var eCashCredentials = req.body;
    sails.log.verbose("Starting charge function..");

    async.waterfall([
      function (waterfallCb) {
        sails.log.verbose("PlatformService :: Charge :: Fetching user checkout information.");

        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
          if (err) {
            return waterfallCb(err);
          } else if (!shop) {
            return waterfallCb("Error");
          } else if (_.isEmpty(_.safe(shop,
              ["metadata.", req.appType, ".checkoutInfo.checkoutShopId"].join(""), {}))) {
            return waterfallCb("Error");
          }

          paymentPlatform = shop.metadata[req.appType].checkoutInfo.paymentPlatform;
          selectedShop = shop.metadata[req.appType].checkoutInfo.checkoutShopId;
          shippingId = shop.metadata[req.appType].checkoutInfo.shippingId;
          waterfallCb();
        });
      },
      function (waterfallCb) {
        if (_.isEmpty(eCashCredentials) && paymentPlatform === "ecash") {
          return process.nextTick(function () {
            waterfallCb({
              type : "badRequest",
              msg : "Invalid crendentials"
            })
          });
        } else if (_.isEmpty(eCashCredentials) && paymentPlatform === "paypal") {
          return process.nextTick(waterfallCb);
        }

        requestHttp.post({
          url : sails.config.ecash.loginUrl,
          formData : {
            api_token : sails.config.ecash.apiToken,
            api_key : sails.config.ecash.apiKey,
            username : eCashCredentials.username,
            password : eCashCredentials.password
          }
        }, function (err, result, body) {
          if (err) {
            return callback(err);
          }

          try {
            body = JSON.parse(body);
          } catch (e) {
            sails.log.error("SessionsController :: _upsSession ::", e.message);
            return callback("Error.");
          }

          if (!body.S) {
            return waterfallCb({
              type : "badRequest",
              msg : body.M
            });
          }

          eCashCredentials.merchantId = body.merchant_id;
          eCashCredentials.merchantName = body.merchant_name;
          waterfallCb();
        });
      },
      function (waterfallCb) {
        sails.log.verbose("PlatformService :: Charge :: Gathering cart items based on selected shop.");

        Cart.find({
          userId : userId,
          shopId : selectedShop,
          status : "added",
          isDeleted : false
        }).populateAll()
          .exec(function (err, items) {
          if (err) {
            return waterfallCb(err);
          } else if (!items.length) {
            return waterfallCb({
              type : "notFound",
              msg : "No items in cart."
            });
          }

          cartItems = items;
          waterfallCb();
        });
      },
      function (callback) {
        sails.log.verbose("PlatformService :: Charge :: Fetching information on selected shop.");

        Shops.findOneById(selectedShop)
          .populateAll()
          .exec(function (err, shop) {
            if (err) {
              return callback(err);
            } else if (!shop) {
              return callback({
                type : "notFound",
                msg : "Seller shop not found."
              });
            }

            selectedShop = shop;
            callback();
          });
      },
      function (callback) {
        sails.log.verbose("PlatformService :: Charge :: Fetching currency information for selected shop.");

        Currencies.findOneById(selectedShop.metadata[req.appType].currencyId)
          .exec(function (err, currency) {
            if (err) {
              return callback(err);
            } else if (!currency) {
              return callback("No currency found.");
            }

            shopCurrency = currency;
            callback();
          });
      },
      // perform quantity checking and do reduction if all is good.
      function (callback) {
        sails.log.verbose("PlatformService :: Charge :: Processing product inventories");

        async.each(cartItems, function (item, eachCb) {
          item.processInventory({}, eachCb);
        }, function (err) {
          if (err) {
            return self.rollBackInventory(cartItems, err, callback);
          }
          callback();
        });
      },
      // perform calculations like taxes, shipping costs and commissions.
      // format data to conform paypal format;
      function (callback) {
        sails.log.verbose("PlatformService :: Charge :: Calculating cost.");

        async.each(cartItems, function (item, eachCb) {
          try {
            item.computeTagCommission({});
            item.computeResellCommission({});
          } catch (e) {
            return eachCb(e);
          }

          item.computeShippingCost({
            shippingId : shippingId,
            selectedShop : selectedShop,
            hasOtherItems : cartItems.length > 1
          }, function (err) {
            if (err) {
              return eachCb(err);
            }

            eachCb();
          });
        }, function (err) {
          if (err) {
            return self.rollBackInventory(cartItems, err, callback);
          }

          callback();
        });
      },
      // charge
      function (callback) {
        sails.log.verbose("PlatformService :: Charge :: Processing payments.");
        var result = null;
        var payFn = null;

        try {
          result = self.processPayments({
            selectedShop : selectedShop,
            shopCurrency : shopCurrency,
            cartItems : cartItems,
            appType : req.appType
          });
        } catch (e) {
          return self.rollBackInventory(cartItems, e, callback);
        }

        switch (paymentPlatform) {
          case "ecash":
            payFn = eCashService.pay;
            break;
          default:
            sails.log.error("PlatformService :: Charge :: Payment platform not set.");
            return self.rollBackInventory(cartItems, e, callback);
        }

        payFn({
          payment : result.payment,
          receivers : result.receivers,
          currencyCode : shopCurrency.code,
          appType : req.appType,
          credentials : eCashCredentials
        }, function (err, payment) {
          if (err) {
            return self.rollBackInventory(cartItems, err, callback);
          }

          receivers = result.receivers;
          callback(null, payment, result.payment);
        });
      },
      function (payment, paymentBreakdown, callback) {
        sails.log.verbose("PlatformService :: Charge :: Generating transaction.");

        var obj = {
          userId : userId,
          receivers : receivers,
          payment : payment,
          payKey : payment.payKey,
          paymentBreakdown : paymentBreakdown,
          paymentPlatform : paymentPlatform,
          appType : req.appType
        };

        if (paymentPlatform === "ecash") {
          obj.status = "complete";
        }

        Transactions.create(obj).exec(function (err, transaction) {
          if (err) {
            return  callback({
              type : "serverError",
              msg : err
            });
          }

          callback(null, transaction, payment);
        });
      },
      function (transaction, payment, callback) {
        sails.log.verbose("PlatformService :: Charge :: Generating order.");

        var randomNum = _.random(99999).toString(36);
        while(randomNum.length < 4) {
          randomNum = "0" + randomNum;
        }

        var obj = {
          userId : userId,
          shopId : selectedShop.id,
          referenceId : (base36() + randomNum).toUpperCase(),
          transactionId : transaction.id,
          payKey : payment.payKey,
          shippingId : shippingId,
          items : cartItems,
          paymentPlatform : paymentPlatform,
          appType : req.appType,
        };

        if (paymentPlatform === "ecash") {
          obj.isCompleted = true;
        }

        Orders.create(obj).exec(function (err, result) {
          if (err) {
            return callback(err);
          }

          callback(null, payment);
        });
      }
    ], function (err, payment) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }
        return res.serverError(err);
      }

      var response = {
        paymentApprovalUrl : payment.paymentApprovalUrl,
        status : "success",
        paymentPlatform : paymentPlatform
      };

      sails.log.verbose("PlatformService :: Charge :: done");
      res.ok(response);
    });
  },

  processPayments : function (options) {
    if (!Utils.checkProps(options, ["selectedShop", "shopCurrency", "cartItems", "appType"])) {
      sails.log.error("Cart :: fetchSeller :: Missing parameters.");
      throw new Error("Missing parameters.");
    }

    var receivers = [];
    var payment = {
      tax : 0,
      totalAmount : 0,
      totalItemsAmount : 0,
      projectedPaypalFee : 0,
      handlingFee : 0,
      totalShipping : 0,
      platformFee : 0
    };
    var defaultCurrency = sails.config[options.appType].defaultCurrency
    var exchangeRate = options.shopCurrency.exchangeRates[defaultCurrency];
    var obj = {
      shopId : options.selectedShop.id,
      amount : _.reduce(options.cartItems, function (acc, item) {
        return acc + item.computation.netReceive
      }, 0),
      type : "sale",
      currency : options.shopCurrency.code,
      primary : true
    };

    obj.platformFee = PlatformService.getFee(obj.amount);
    receivers.push(obj);

    //get tagger/reseller shop if any
    _.each(options.cartItems, function (item) {
      var commissionReceiverObj = {
        productId : item.productId.id,
        amount : 0,
        primary : false,
        type : "commission",
        currency : options.shopCurrency.code
      };

      if (item.resoldBy) {
        commissionReceiverObj.amount = item.computation.resellCommission;
        commissionReceiverObj.shopId = item.resoldBy.id;
        commissionReceiverObj.commissionType = "resell"
        receivers.push(commissionReceiverObj);
      } else if (item.tagger) {
        commissionReceiverObj.amount = item.computation.tagCommission;
        commissionReceiverObj.shopId = item.tagger.id;
        commissionReceiverObj.commissionType = "tag"
        receivers.push(commissionReceiverObj);
      }
    });

    // Items total prices
    payment.totalItemsAmount += _.reduce(options.cartItems, function (acc, item) {
      return acc + item.computation.grossAmount;
    }, 0);
    payment.totalItemsAmount = Math.ceil(payment.totalItemsAmount * 100) / 100;

    // Items total shipping cost
    payment.totalShipping += _.reduce(options.cartItems, function (acc, item) {
      return acc + item.computation.shippingCost;
    }, 0);

    //Round shipping cost
    payment.totalShipping = Math.ceil(payment.totalShipping * 100) / 100;
    payment.totalAmount = payment.totalItemsAmount + payment.totalShipping + payment.tax;

    // Handle percentage platform fee rounding correcty
    if (options.shopCurrency.nonDecimal) {
      payment.platformFee = Math.ceil(payment.totalAmount * (sails.config[options.appType].platformFee.percentage / 100));
    } else {
      payment.platformFee = Math.ceil(payment.totalAmount * sails.config[options.appType].platformFee.percentage) / 100;
    }

    // Round fixed platform fee
    payment.platformFee += Math.ceil((sails.config[options.appType].platformFee.flat / exchangeRate) * 100) / 100;

    payment.totalAmount += payment.platformFee;

    return {
      receivers : receivers,
      payment : payment
    };
  },

  rollBackInventory : function (items, causingErr, callback) {
    sails.log.error("PlatformService :: rollBackInventory :: Error occurred. performing rollback.");
    sails.log.error("PlatformService :: rollBackInventory :: Causing error: ", causingErr);

    async.each(items, function (item, eachCb) {
      Cart.update({
        id : item.id,
        status : "prepurchased",
        isDeleted : false,
      }, {
        status : "added"
      }).exec(function (err, result) {
        if (err) {
          return eachCb(err);
        } else if (!result.length) {
          return eachCb();
        }
        Products.findOne({
          id : item.productId.id,
          isDeleted : false
        }).exec(function (err, prod) {
          if (err) {
            return eachCb(err);
          }
          prod.quantity += item.quantity;
          prod.save(function (err, prodResult) {
            if (err) {
              return eachCb(err);
            }
            eachCb(null, prodResult);
          });
        });
      });
    }, function (err) {
      if (err) {
        sails.log.error("PlatformService :: rollBackInventory :: Wtf! rollback failed. Panic!");
        return callback(err);
      } else if (causingErr.type === "insufficientFunds") {
        return callback({
          type : "badRequest",
          msg : causingErr.msg
        });
      }
      callback("Error occurred, rollback done.");
    });
  }
}
