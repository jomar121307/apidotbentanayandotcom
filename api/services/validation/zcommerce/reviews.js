module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Z-Commerce :: Reviews");
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Validations :: Z-Commerce :: Reviews :: App type is undefined.");
  } else if (isUpdate) {
    return callback();
  }

  _.ensure(values, "metadata.likeCount", 0);
  callback();
};
