var ObjectID = require("objectid");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Global :: Reviews");
  sails.log.verbose(values);

  sails.log.verbose(_.keys(values));

  if (!values.appType) {
    throw new Error("Validations :: Global :: Reviews :: App type is undefined.");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        process.nextTick(seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }

  async.series([
    function (seriesCb) {
      checkIfAlreadyReviewed(values, seriesCb);
    },
    function (seriesCb) {
      if (!values.isReply) {
        return process.nextTick(seriesCb);
      }

      checkReviewReply(values, seriesCb);
    },
    function (seriesCb) {
      if (values.isReply) {
        return process.nextTick(seriesCb);
      }

      checkProduct(values, seriesCb);
    },
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Checks review if it is a reply
 * if not, skip; if reply, check if review is valid
 *
 * @method     checkReview
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkReviewReply (values, callback) {
  if (!(values.isReply && ObjectID.isValid(values.reviewId || ""))) {
    return process.nextTick(callback);
  } else if (values.isReply) {
    return callback(ErrorService.validation({
      reviewId : [
        {
          rule : "valid",
          message : "Invalid reviewId"
        }
      ]
    }));
  } else if (values.reviewId) {
    return callback(ErrorService.validation({
      isReply : [
        {
          rule : "required",
          message : "Add an isReply attribute."
        }
      ]
    }))
  }

  Reviews.findOne({
    id : values.reviewId,
    appType : values.appType,
    isDeleted : false
  }).exec(function (err, foundReview) {
    if (err) {
      return callback(err);
    } else if (!foundReview) {
      return callback(ErrorService.validation({
        reviewId : [
          {
            rule : "valid",
            message : "Review not found."
          }
        ]
      }));
    }

    callback();
  });
}

/**
 * Check if product is valid
 *
 * @method     checkProduct
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkProduct (values, callback) {
  Products.findOne({
    id : values.productId,
    appType : values.appType,
    isDeleted : false
  }).exec(function (err, foundProduct) {
    if (err) {
      return callback(err);
    } else if (!foundProduct) {
      return callback(ErrorService.validation({
        productId : [
          {
            rule : "valid",
            message : "Product not found."
          }
        ]
      }));
    }

    callback();
  });
}

/**
 * Checks if user has already reviewed the product.
 * throw tantrum if isReply is true
 *
 * @method     checkAlreadyReviewed
 * @param      {<type>}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkIfAlreadyReviewed (values, callback) {
  Reviews.findOne({
    userId : values.userId,
    productId : values.productId,
    isReply : false,
    isDeleted :false
  }).exec(function (err, foundReview) {
    if (err) {
      return callback(err);
    } else if (foundReview) {
      return callback(ErrorService.validation({
        review : [
          {
            rule : "once",
            message : "You can only review a product once."
          }
        ]
      }));
    }

    callback()
  });
}
