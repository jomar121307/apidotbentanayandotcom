module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Global :: Shops");
  sails.log.verbose(values);

  var currencyCode = "USD";

  if (!isUpdate) {
    _.each(sails.config.appList, function (appName) {
      _.ensure(values, "metadata." + appName, {});
      _.extend(values.metadata[appName], {
        currencyId : "",
        shopName : "",
        productCount : 0,
        feedbackScore : 0
      });
    });
  }

  if (values.appOrigin === "zcommerce") {
    currencyCode = "PHP";
  }

  Currencies.find({
    code : ["USD", "PHP"]
  }).exec(function (err, foundCurrencies) {
    if (err) {
      return callback(err);
    } else if (foundCurrencies.length !== 2) {
      sails.log.error("Validation :: Global :: Shops :: Currencies not found.");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    if (foundCurrencies[0].code === "PHP") {
      values.metadata.zcommerce.currencyId = foundCurrencies[0].id;
      //values.metadata.petgago.currencyId = foundCurrencies[1].id;
     // values.metadata.golfgago.currencyId = foundCurrencies[1].id;
     // values.metadata.musicgago.currencyId = foundCurrencies[1].id;
    } else if (foundCurrencies[0].code === "USD") {
      values.metadata.zcommerce.currencyId = foundCurrencies[1].id;
      values.metadata.petgago.currencyId = foundCurrencies[0].id;
      values.metadata.golfgago.currencyId = foundCurrencies[0].id;
      values.metadata.musicgago.currencyId = foundCurrencies[0].id;
    } else {
      return callback({
        type : "serverError"
      });
    }

    callback();
  });
};
