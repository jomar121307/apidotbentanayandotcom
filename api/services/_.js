var lodash = require("lodash");
var underscore_string = require("underscore.string");

//sails conflicting property.
delete lodash.identity;

lodash.mixin(require("safe-obj"));
lodash.mixin(underscore_string.exports());

module.exports = lodash;
