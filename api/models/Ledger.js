/**
* Ledger.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    shopId : {
      model : "shops",
      required : true
    },

    transactionId : {
      model : "transactions",
      required : true
    },

    amount : {
      type : "float",
      required : true
    },

    type : {
      type : "string",
      enum : ["sale", "commission", "platformFee"],
      required : true
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    }
  }
};

