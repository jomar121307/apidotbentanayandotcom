/**
* Messages.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    roomId : {
      model : "rooms",
      required : true
    },

    to : {
      model : "users",
      required : true
    },

    from : {
      model : "users",
      required : true
    },

    payload : {
      type : "json",
      required : true
    },

    metadata : {
      type : "json"
    },

    status : {
      type : "string",
      enum : ["read", "unread"],
      defaultsTo : "unread"
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var msg = this.toObject();

      delete msg.appType;
      delete msg.isDeleted;

      return msg;
    }
  },

  afterCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ModelHookService.global.messages(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }
};

