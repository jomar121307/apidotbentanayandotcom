/**
* Currencies.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    currency : {
      type : "string",
      required : true
    },

    code : {
      type : "string",
      required : true
    },

    symbol : {
      type : "string",
      required : true
    },

    exchangeRates : {
      type : "json"
    },

    nonDecimal : {
      type : "boolean",
      defaultsTo : false
    },

    flatRateFee : {
      type : "float",
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var currency = this.toObject();
      return _.pick(currency, ["code", "currency", "symbol", "exchangeRates"]);
    }
  }
};

