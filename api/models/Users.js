/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    email : {
      type : "email",
      maxLength : 1024
    },

    username : {
      type : "string",
      maxLength : 1024
    },

    password : {
      type : "string",
    },

    firstName : {
      type : "string",
      maxLength : 64,
      required : true
    },

    middleName : {
      type : "string",
      maxLength : 64
    },

    lastName : {
      type : "string",
      maxLength : 64
    },

    gender : {
      type : "string",
      enum : ["male", "female"]
    },

    birthday : {
      type : "date"
    },

    location : {
      type : "json",
      defaultsTo : {}
    },

    profilePhoto : {
      type : "json",
      defaultsTo : {}
    },

    coverPhoto : {
      type : "json",
      defaultsTo : {}
    },

    fbId : {
      type : "string"
    },

    twitterId : {
      type : "string"
    },

    googleId : {
      type : "string"
    },

    notificationCount : {
      type : "integer",
      defaultsTo : 0
    },

    pageCount : {
      type : "integer",
      defaultsTo : 0
    },

    metadata : {
      type : "json"
    },

    currencyId : {
      model : "currencies"
    },

    registrationType : {
      type : "string",
      enum : ["facebook", "twitter", "native", "upsexpress"],
      required: true
    },

    appOrigin : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    randomPoint : {
      type : "array",
      defaultsTo : function () {
        return [Math.random(), Math.random()]
      }
    },

    isAdmin : {
      type : "boolean",
      defaultsTo : false
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    resetPasswordToken : {
      type : "string"
    },

    getNameAndId : function () {
      return {
        name : this.name,
        id : this.id
      };
    },

    profileSanitize : function () {
      var obj = this.toObject();
      return {
        firstName : obj.firstName,
        middleName : obj.middleName,
        lastName : obj.lastName,
        birthdate : obj.birthdate,
        gender : obj.gender,
        location : obj.location,
        email : obj.email,
        metadata : obj.metadata,
        profilePhoto : obj.profilePhoto,
        coverPhoto : obj.coverPhoto,
        createdAt : obj.createdAt,
        id : obj.id
      };
    },

    //TODOS: add user info permissions.
    sanitize : function () {
      var user = this.toObject();

      if (!user.isAdmin) {
        delete user.isAdmin;
      }

      delete user.password;
      delete user.fbId;
      delete user.twitterId;
      delete user.googleId;
      delete user.notificationCount;
      delete user.pageCount;
      delete user.registrationType;
      delete user.appOrigin;
      delete user.randomPoint;
      delete user.isDeleted;

      return user;
    },

    /**
     * Adds isBlocked property for each user object.
     *
     * @param options.userId {String}
     * @param options.users {JSON Array}
     * @returns {Array} Returns user array
     *
     */
    checkBlocked : function (options, callback) {
      var user = this;

      if (!Utils.checkProps(options, ["userId", "appType"])) {
        sails.log.error("Users :: checkBlocked :: Missing parameters")
        return callback({
          type : "serverError",
          msg : ""
        });
      } else if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      }

      Blocked.findOne({
        blockerId : options.userId,
        blockedId : user.id,
        appType : options.appType,
        isDeleted : false
      }).exec(function (err, blockedUser) {
        if (err) {
          return callback(err);
        } else if (blockedUser) {
          user.isBlocked = true;
          return callback();
        }

        user.isBlocked = false;
        callback();
      });
    }
  },

  verifyPassword : function(candidatePassword, thisPassword, cb) {
    CryptoService.compareHash(candidatePassword, thisPassword, cb);
  },

  beforeCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ValidationService.global.users(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      } else if (values.registrationType !== "native" && values.registrationType !== "upsexpress") {
        return callback();
      }

      CryptoService.getHash(values.password, function (err, hash) {
        if (err) {
          return callback(err);
        }

        values.password = hash;
        callback();
      });
    });
  },

  afterCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ModelHookService.global.users(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    })
  },

  beforeUpdate : function (values, callback) {
    this.beforeCreate(values, callback, true);
  },

  afterUpdate : function (values, callback) {
    this.afterCreate(values, callback, true);
  },

  /**
   * Find pages owned by user.
   *
   * @param {object} options
   * @param {string} options.userId
   * @param {string} options.appType
   * @param {object} [options.where]
   * @param {number} [options.skip]
   * @param {number} [options.limit]
   * @param {string} [options.sort]
   * @param {function} nodejs callback
   * @returns {object} Cursor
   * @returns {number} Cursor.count
   * @returns {array} Cursor.pages
   *
   */
  findOwnedPages : function (options, callback) {
    if (!Utils.checkProps(options, ["userId", "appType"])) {
      sails.log.error("Users :: findOwnedPages :: Missing parameters")
      return callback({
        type : "serverError",
        msg : ""
      });
    } else if (typeof callback !== "function") {
      throw new Error("Callback is not a function.");
    }

    try {
      if (options.where) {
        options.where = JSON.parse(optionst.where);
      } else {
        options.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return callback({
        type : "badRequest",
        msg : "Malformed query"
      });
    }

    Pages.count({
      userId : options.userId,
      appType : options.appType,
      isDeleted : false
    }).exec(function (err, pageCount) {
      if (err) {
        return callback(err);
      } else if (!pageCount) {
        return callback(null, {
          count : 0,
          pages : []
        });
      }

      Pages.find({
        userId : options.userId,
        appType : options.appType,
        isDeleted : false
      }).skip(options.skip)
        .limit(options.limit)
        .where(options.where)
        .sort(options.sort)
        .populate("userId")
        .exec(function (err, foundPages) {
          if (err) {
            return callback(err);
          }

          callback(null, {
            count : pageCount,
            pages : foundPages
          });
        });
    });
  },

  /**
   * Find pages followed by user.
   *
   * @param {object} options
   * @param {string} options.userId
   * @param {string} options.appType
   * @param {object} [options.where]
   * @param {number} [options.skip]
   * @param {number} [options.limit]
   * @param {string} [options.sort]
   * @param {function} nodejs callback
   * @returns {object} Cursor
   * @returns {number} Cursor.count
   * @returns {array} Cursor.pages
   *
   */
  findFollowedPages : function (options, callback) {
    if (!Utils.checkProps(options, ["userId", "appType"])) {
      sails.log.error("Users :: findFollowedPages :: Missing parameters")
      return callback({
        type : "serverError",
        msg : ""
      });
    } else if (typeof callback !== "function") {
      throw new Error("Callback is not a function.");
    }

    try {
      if (options.where) {
        options.where = JSON.parse(optionst.where);
      } else {
        options.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return callback({
        type : "badRequest",
        msg : "Malformed query"
      });
    }

    _.extend(options.where, {
      userId : options.userId,
      appType : options.appType,
      isOwner : false,
      isDeleted : false
    });

    async.waterfall([
      function (waterfallCb) {
        Follows.count()
          .where(options.where)
          .exec(function (err, pageCount) {
            if (err) {
              return waterfallCb(err);
            } else if (!pageCount) {
              return waterfallCb({
                type : "skip",
                msg : {
                  count : pageCount,
                  pages : []
                }
              });
            }

            waterfallCb(null, pageCount);
          });
      },
      function (pageCount, waterfallCb) {
        Follows.find()
          .where(options.where)
          .skip(options.skip)
          .limit(options.limit)
          .exec(function (err, followedPages) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, {
              count : pageCount,
              pages : followedPages
            });
          });
      },
      function (payload, waterfallCb) {
        async.map(payload.pages, function (page, mapCb) {
          Pages.findOne({
            id : page.entityId,
            isDeleted : false
          }).exec(function (err, foundPage) {
            if (err) {
              return mapCb(err);
            }

            mapCb(null, foundPage);
          });
        }, function (err, mapResult) {
          if (err) {
            return waterfallCb(err);
          }

          payload.pages = mapResult;
          waterfallCb(null, payload);
        });
      }
    ], function (err, waterfallResult) {
      if (err && err.type === "skip") {
        return callback(null, err.msg);
      } else if (err) {
        return callback(err);
      }

      callback(null, waterfallResult);
    });
  }
};

