/**
* Misc.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    name : {
      type : "string",
      required : true
    },

    metadata : {
      type : "json",
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    sanitize : function () {
      var misc = this.toObject();

      delete misc.appType;
      delete misc.id;
      delete misc.updatedAt;
      delete misc.createdAt;

      return misc;
    }
  }
};

