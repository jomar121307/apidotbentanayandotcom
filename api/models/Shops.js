/**
* Shops.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var ObjectID = require("objectid");

module.exports = {
  schema : true,

  attributes: {
    userId : {
      model : "users",
      required : true
    },

    metadata : {
      type : "json",
      defaultsTo : {}
    },

    randomPoint : {
      type : "array",
      defaultsTo : [Math.random(), Math.random()]
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function (appType) {
      var shop = this.toObject();

      if (!appType) {
        sails.log.error("Shops :: sanitize :: App type is undefined.");
        throw new Error();
      }

      // shop.metadata = shop.metadata[appType];

      delete shop.metadata;
      delete shop.randomPoint;
      delete shop.isDeleted;
      return shop;
    },

    /**
     * Populates the currencyId for this shop.
     *
     * @param {object} options
     * @param {string} options.appType
     * @param {function} nodejs callback
     *
     */
    populateCurrency : function (options, callback) {
      var shop = this;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, ["appType"])) {
        sails.log.error("Shops :: populateCurrency :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      Currencies.findOne({
        id : shop.metadata[options.appType].currencyId,
        isDeleted : false
      }).exec(function (err, foundCurrency) {
        if (err) {
          return callback(err);
        } else if (!foundCurrency) {
          sails.log.error("Shops :: populateCurrency :: Currency not found.", shop.metadata[options.appType].currencyId)
          return callback({
            type : "serverError",
            msg : ""
          });
        }

        shop.metadata[options.appType].currencyId = foundCurrency.sanitize();
        callback();
      });
    },

    /**
     * Fetch products of a shop.
     *
     * @param {object} options
     * @param {string} options.appType
     * @param {object} [options.where]
     * @param {number} [options.skip]
     * @param {number} options.limit
     * @param {string} [options.sort]
     * @param {function} nodejs callback
     *
     */
    fetchProducts : function (options, callback) {
      var shop = this;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, ["limit"])) {
        sails.log.error("Pages :: fetchPosts :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      try {
        if (options.where) {
          options.where = JSON.parse(options.where);
        } else {
          options.where = {};
        }
      } catch (e) {
        sails.log.error(e);
        return callback({
          type : "badRequest",
          msg : "Malformed query"
        });
      }

      Products.find({
        shopId : shop.id,
        appType : options.appType,
        isDeleted : false
      }).limit(options.limit)
        .skip(options.skip)
        .sort(options.sort)
        .where(options.where)
        .populate("brandId")
        .exec(function (err, foundProducts) {
          if (err) {
            return callback(err);
          }

          foundProducts = _.map(foundProducts, function (product) {
            product.brandId = product.brandId.sanitize();
            return product.sanitize();
          });

          shop.products = foundProducts;
          callback();
        });
    }
  },

  //START LIFECYCLE CALLBACKS

  beforeCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ValidationService.global.shops(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  },

  beforeUpdate : function (values, callback) {
    this.beforeCreate(values, callback, true);
  },

  //END LIFECYCLE CALLBACKS

  /**
   * Find random pages.
   *
   * @param {String} options.userId
   * @param {String} options.appType
   * @param {Object} options.where
   * @param {Number} [options.skip]
   * @param {Number} [options.limit]
   * @param {String} [options.sort]
   * @param {Function} nodejs callback
   * @returns {Object} Cursor
   * @returns {Number} Cursor.count
   * @returns {Array} Cursor.pages
   *
   */
  findRandom : function (options, callback) {
    if (typeof callback !== "function") {
      throw new Error("Callback is not a function.");
    } else if (!Utils.checkProps(options, ["userId", "appType", "where"])) {
      sails.log.error("Shops :: findRandom :: Missing parameters.");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    try {
      if (options.where) {
        options.where = JSON.parse(options.where);
      } else {
        options.where = {};
      }

      if (options.sort) {
        options.sort = JSON.parse(options.sort);
      } else {
        options.sort = {};
      }
    } catch (e) {
      sails.log.error(e);
      return callback({
        type : "badRequest",
        msg : "Malformed query"
      });
    }

    if (!options.where.xRandom || !options.where.yRandom) {
      return callback({
        type : "badRequest",
        msg : "Lacking random points. Add xRandom and yRandom to where with values [0,1)."
      });
    }

    async.waterfall([
      function (waterfallCb) {
        Shops.native(function (err, collection) {
          if (err) {
            return waterfallCb(err);
          }

          collection.count({
            userId : {
              $nin : [ObjectID(options.userId)]
            },
            isDeleted : false
          }, function (err, count) {
            if (err) {
              return waterfallCb(err);
            }

            collection.find({
              randomPoint : {
                $near : [options.where.xRandom, options.where.yRandom]
              },
              userId : {
                $nin : [ObjectID(options.userId)]
              },
              isDeleted : false
            }, {
              skip : options.skip,
              limit : options.limit,
              sort : options.sort
            }).toArray(function (err, foundShops) {
              if (err) {
                return waterfallCb(err);
              }

              waterfallCb(null, {
                count : count,
                shops : foundShops
              });
            });
          });
        });
      },
      function (payload, waterfallCb) {
        if (!payload.count) {
          return process.nextTick(function () {
            waterfallCb({
              type : "skip",
              data : payload
            });
          });
        }

        async.map(payload.shops, function (shop, mapCb) {
          Shops.findOne({
            id : shop._id + "",
            isDeleted : false
          }).populate("userId")
            .exec(function (err, foundShop) {
              if (err) {
                return mapCb(err);
              } else if (!foundShop) {
                sails.log.info("Shops :: findRandom :: Shop not found; maybe deleted.", shop._id);
                return mapCb();
              }

              foundShop.populateCurrency({
                appType : options.appType
              }, function (err) {
                if (err) {
                  return mapCb(err);
                }

                mapCb(null, foundShop);
              });
            });
        }, function (err, mapResult) {
          if (err) {
            return waterfallCb(err);
          }

          payload.shops = _.compact(mapResult);
          waterfallCb(null, payload);
        });
      }
    ], function (err, waterfallResult) {
      if (err && err.type === "skip") {
        return callback(null, err.data);
      } else if (err) {
        return callback(err);
      }

      callback(null, waterfallResult);
    });
  },

  /**
   * Fetches the paypal information of this shop.
   *
   * @param {String} options.userId
   * @param {String} options.appType
   * @param {Function} nodejs callback
   * @returns {Object} Paypal information
   *
   */
  fetchPaypalInfo : function (options, callback) {
    if (typeof callback !== "function") {
      throw new Error("Callback is not a function.");
    } else if (!Utils.checkProps(options, ["userId", "appType"])) {
      sails.log.error("Shops :: fetchPaypalInfo :: Missing parameters.");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    Shops.findOne({
      userId : options.userId,
      isDeleted : false
    }).exec(function (err, foundShop) {
      if (err) {
        return callback(err);
      } else if (!foundShop) {
        return callback({
          type : "notFound",
          msg : "Shop not found."
        });
      } else if (_.safe(foundShop, "metadata." + options.appType + ".paypalInfo.accountType") !== "BUSINESS") {
        return callback({
          type : "badRequest",
          msg : "Please setup a Paypal business account before reselling products."
        });
      }

      callback(null, foundShop.metadata[options.appType].paypalInfo);
    });
  }
};
