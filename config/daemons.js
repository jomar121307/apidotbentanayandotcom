module.exports.daemons = {
  payments : {
    cronTime : process.env.NODE_ENV === "production" ? "*/10 * * * * *" : "00 * * * * *"
  },
  exchangeRate : {
    cronTime : "00 00 * * * *"
  },
  cargoTasks : 10
}
